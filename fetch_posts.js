function createTag(tag, attrs) {
  var t = document.createElement(tag);
  for(k in attrs) {
    t.setAttribute(k, attrs[k]);
  }
  return t;
}

function container(selector) {
  return document.querySelector(selector);
}

function tail_maker(e) { return "?type="+e+""; }

function fetch_posts(type, selector, num) {
  var url = "https://feeds.420cloud.com/posts";
  var num = num || 5;
  var type = type || 'recent';
  var tail = "?type="+type+"&num_of_posts="+num+"";
  url +=tail;
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200) {
      var data = JSON.parse(this.responseText);
      container(selector).innerHTML = "";
      data["data"].forEach(function(e){
        var p = createTag("li", {class: "footer-menu-item footer-menu-news"});
        var a = createTag("a", {title: e.title, href: e.guid.replace(/^http:\/\//i, 'https://'), target: "_blank", class: "footer-menu-link"});
        var div = createTag("div", {class: "footer-menu-block"});
        var pic = createTag("img", {alt: e.title, src: e.thumbnail_url.replace(/^http:\/\//i, 'https://'), class: "footer-menu-img", width:"40", height:"40"});
        var anchor_text = document.createTextNode(e.title);
        a.appendChild(div);
        div.appendChild(pic);
        a.appendChild(anchor_text);
        p.appendChild(a);
        container(selector).appendChild(p);
      });
    }
  }
  xhr.open("GET", url, true);
  xhr.send();
}

function fetch_headlines_to(selector) {
  var url = "https://feeds.420cloud.com/posts?type=headline";
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
        var data = JSON.parse(this.responseText);
        container(selector).innerHTML = "";
        data["data"].forEach(function(e){
          var li = createTag("li", {id: e.id, class: "swiper-slide"});
          var cat_anchor = createTag("a", {href: e.guid.split("?")[0]+"category/"+e.cat_name+"", class: "title-slide cat-title-slide", title: e.cat_name});
          var cat_anchor_text = document.createTextNode(e.cat_name);
          var hl_anchor = createTag("a", {class: "title-slide", href:e.guid, title: e.title});
          var hl_anchor_text = document.createTextNode(e.title);
          cat_anchor.appendChild(cat_anchor_text);
          hl_anchor.appendChild(hl_anchor_text);
          [cat_anchor, hl_anchor].forEach(e => li.appendChild(e))
          container(selector).appendChild(li);
        });
    }
  }

  xhr.open("GET", url, true);
  xhr.send();
}


function fetch_stocks_to(selector) {
  function main() {
    var url = "https://feeds.420cloud.com/stocks";
    var static = {
        profile: "http://www.otcmarkets.com/stock/MCIG/profile",
        title: "OTCQB: MCIG"
      };
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
        var data = JSON.parse(this.responseText);
        container(selector).innerHTML = "";
        var anchor = createTag("a", {href: static.profile, title: static.title, target: "_blank"});
        var sp_for_value = createTag("span", {class: "stock_symbol"});
        var span_text = document.createTextNode(data.t);
        var a_text = document.createTextNode(data.l);
        decision_maker = function(e) { return parseFloat(e) >= 0 ? "green" : "red"; }
        ch_class = "stock-"+decision_maker(data.c);
        var ch_for_value = createTag("span", {class: ch_class});
        var ch_value_text = document.createTextNode(data.c + "( " + data.cp+ "% )");
        sp_for_value.appendChild(span_text);
        ch_for_value.appendChild(ch_value_text);
        anchor.appendChild(sp_for_value);
        anchor.appendChild(a_text);
        anchor.appendChild(ch_for_value);
        container(selector).appendChild(anchor);
      }
    }
    xhr.open("GET", url, true);
    xhr.send();
  }
  setInterval(main, 15*60*1000);
}

function fetch_posts_as_widget(selector, num, type) {
  var tail = tail_maker(type);
  var url = "https://feeds.420cloud.com/posts"+tail;
  var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
        var data = JSON.parse(this.responseText);
        container(selector).innerHTML = "";
        data["data"].forEach(function(e){
          var main_div = createTag("div", {class: "blog-item margin-bottom-20" });
          var anchor = createTag("a", {target: "_blank", href: e.guid});
          var img_holder = createTag("div", {class: "blog-image-holder", style: "background-image: url('"+e.thumbnail_url+"')"});
          var img_placeholder = createTag("div", {class: "blog-image-placeholder"});
          var title = createTag("p", {class: "blog-title"});
          var title_text = document.createTextNode(e.title);
          img_holder.appendChild(img_placeholder);
          title.appendChild(title_text);
          [image_holder, title].forEach(e=> anchor.appendChild(e));
          main_div.appendChild(anchor);
          selector.appendChild(main_div);
        }
      }  
    }
  xhr.open("GET", url, true);
  xhr.send();
}


